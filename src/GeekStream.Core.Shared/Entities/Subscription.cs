namespace GeekStream.Core.Shared.Entities
{
    public class Subscription
    {
        public int Id { get; set; }
        public AppUser Subscriber { get; set; }
        public int CategoryId { get; set; }
        public string AuthorId { get; set; }
        public string Type { get; set; }
    }
}