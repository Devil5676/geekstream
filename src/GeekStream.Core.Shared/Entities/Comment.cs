namespace GeekStream.Core.Shared.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public Article Article { get; set; }
        public AppUser Author { get; set; }
        public string Text { get; set; }
    }
}