namespace GeekStream.Core.Shared.Entities
{
    public class ReviewApproval
    {
        public int Id { get; set; }
        public Review Review { get; set; }
        public AppUser Reviewer { get; set; }
    }
}