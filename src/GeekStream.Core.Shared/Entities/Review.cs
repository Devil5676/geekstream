using System;
using System.Collections.Generic;

namespace GeekStream.Core.Shared.Entities
{
    public class Review
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public DateTime ReviewCreated { get; set; }
        public AppUser Author { get; set; }
        public Article Article { get; set; }
        public ICollection<ReviewApproval> Approvals { get; set; }
        public ICollection<ReviewComment> Comments { get; set; }
    }
}