using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace GeekStream.Core.Shared.Entities
{
    public class AppUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public ICollection<Article> Articles { get; set; }
        
        public ICollection<Subscription> Subscriptions { get; set; }

        public string AvatarPath { get; set; }
    }
}