using System.Collections.Generic;

namespace GeekStream.Core.Shared.Entities
{
    public class Keyword
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public IEnumerable<Article> Articles { get; set; }
    }
}