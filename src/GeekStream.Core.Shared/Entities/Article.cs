using System;
using System.Collections.Generic;

namespace GeekStream.Core.Shared.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
        public bool IsDraft { get; set; }
        public AppUser Author { get; set; }
        public Category Category { get; set; }
        public ICollection<Keyword> Keywords { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<ArticleVote> ArticleVotes { get; set; }
        public DateTime? PublishedDate { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    }
}