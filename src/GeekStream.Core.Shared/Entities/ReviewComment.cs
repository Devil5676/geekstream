namespace GeekStream.Core.Shared.Entities
{
    public class ReviewComment
    {
        public int Id { get; set; }
        public Review Review { get; set; }
        public AppUser Author { get; set; }
        public string Text { get; set; }
    }
}