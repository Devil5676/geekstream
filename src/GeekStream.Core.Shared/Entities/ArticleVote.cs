namespace GeekStream.Core.Shared.Entities
{
    public class ArticleVote
    {
        public int Id { get; set; }
        public Article Article { get; set; }
        public AppUser VotingUser { get; set; }
        public bool IsPositive { get; set; } 
    }
}