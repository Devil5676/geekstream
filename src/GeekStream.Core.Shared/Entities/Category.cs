using System.Collections.Generic;

namespace GeekStream.Core.Shared.Entities
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IconPath { get; set; }
        public string BackgroundPath { get; set; }
        public IEnumerable<Article> Articles { get; set; }
    }
}