using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Shared.Repositories
{
    public interface ISubscriptionRepository
    {
        void RemoveSubscription(Subscription subscription);
        
        void SaveChanges();
    }
}