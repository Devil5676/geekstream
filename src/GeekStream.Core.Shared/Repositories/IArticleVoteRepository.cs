using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Shared.Repositories
{
    public interface IArticleVoteRepository
    {
        public void DeleteArticleVote(ArticleVote articleVote);
        public ArticleVote GetArticleVoteByUser(AppUser user, int articleId);
    }
}