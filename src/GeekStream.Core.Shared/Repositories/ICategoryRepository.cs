using System.Collections.Generic;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Shared.Repositories
{
    public interface ICategoryRepository
    {
        public IEnumerable<Category> GetCategories();
        public Category GetCategoryById(int id);
    }
}