using System.Collections.Generic;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Shared.Repositories
{
    public interface IReviewRepository
    {
        public int CreateReview(Review review);
        public Review GetReviewById(int id);
        public ICollection<Review> GetUserReviews(AppUser user);
        public ICollection<Review> GetAllReviews();
        public Review GetReviewByArticleId(int id);
        public void RemoveReview(Review review);
        public void SaveChanges();
    }
}