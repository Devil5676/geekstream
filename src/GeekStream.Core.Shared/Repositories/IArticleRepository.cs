using System.Collections.Generic;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Shared.Repositories
{
    public interface IArticleRepository
    {
        IEnumerable<Article> GetArticles();
        IEnumerable<Article> SearchArticles(string search);
        Article GetArticleById(int id);
        Article GetArticleByIdForRating(int id);

        IEnumerable<Article> GetUserDrafts(AppUser user);
        void AddArticle(Article article);
        void RemoveArticle(Article article);

        void SaveChanges();
    }
}