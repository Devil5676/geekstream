using System.Collections.Generic;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Shared.Repositories
{
    public interface IKeywordRepository
    {
        public Keyword GetKeywordByName(string name);
    }
}