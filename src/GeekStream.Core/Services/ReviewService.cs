using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using Microsoft.AspNetCore.Identity;

namespace GeekStream.Core.Services
{
    public class ReviewService
    {
        private readonly IReviewRepository _reviewRepository;
        private readonly IArticleRepository _articleRepository;
        private readonly UserManager<AppUser> _userManager;

        public ReviewService(
            IReviewRepository reviewRepository,
            IArticleRepository articleRepository,
            UserManager<AppUser> userManager)
        {
            _reviewRepository = reviewRepository;
            _articleRepository = articleRepository;
            _userManager = userManager;
        }

        public Review GetReviewById(int id)
        {
            return _reviewRepository.GetReviewById(id);
        }
        
        public Review GetReviewByArticleId(int id)
        {
            return _reviewRepository.GetReviewByArticleId(id);
        }

        public async Task<ICollection<Review>> GetUserReviews(ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            var reviews = _reviewRepository.GetUserReviews(user);
            return reviews;
        }

        public ICollection<Review> GetAllReviews()
        {
            return _reviewRepository.GetAllReviews();
        }

        public int CreateReview(int articleId)
        {
            var article = _articleRepository.GetArticleById(articleId);
            article.IsDraft = false;
            Review review = _reviewRepository.GetReviewByArticleId(articleId);
            if (review == null)
            {
                review = new Review
                {
                    Author = article.Author,
                    Status = "Ожидает рассмотрения",
                    Article = article,
                    ReviewCreated = DateTime.UtcNow,
                    Comments = new List<ReviewComment>(),
                    Approvals = new List<ReviewApproval>()
                };   
                return _reviewRepository.CreateReview(review);
            }
            review.Author = article.Author;
            review.Status = "Ожидает рассмотрения";
            review.Article = article;
            review.ReviewCreated = DateTime.UtcNow;
            review.Comments.Clear();
            review.Approvals.Clear();
            _reviewRepository.SaveChanges();

            return review.Id;
        }

        public void RejectReview(int reviewId)
        {
            var review = GetReviewById(reviewId);
            review.Article.IsDraft = true;
            review.Status = "Отклонена";
            review.Approvals.Clear();
            _reviewRepository.SaveChanges();
        }

        public async Task ApproveReview(int reviewId, ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            var review = GetReviewById(reviewId);
            review.Approvals.Add(new ReviewApproval
            {
                Reviewer = user
            });
            review.Status = "Подтверждена";
            _reviewRepository.SaveChanges();
        }

        public void RemoveReview(Review review)
        {
            _reviewRepository.RemoveReview(review);
        }
    }
}