using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace GeekStream.Core.Services
{
    public class SubscriptionService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly ISubscriptionRepository _subscriptionRepository;

        public SubscriptionService(
            UserManager<AppUser> userManager,
            ISubscriptionRepository subscriptionRepository)
        {
            _userManager = userManager;
            _subscriptionRepository = subscriptionRepository;
        }
        public void SubscribeCategory(ClaimsPrincipal claimsPrincipal, int id)
        {
            var user = _userManager.Users
                .Include(x => x.Subscriptions)
                .FirstOrDefault(x => x.UserName == claimsPrincipal.Identity.Name);
            if (user == null) return;
            user.Subscriptions.Add(new Subscription
            {
                CategoryId = id,
                AuthorId = null,
                Type = "Category"
            });
            _subscriptionRepository.SaveChanges();
        }
        public async Task SubscribeAuthor(ClaimsPrincipal claimsPrincipal, string id)
        {
            var user = _userManager.Users
                .Include(x => x.Subscriptions)
                .FirstOrDefault(x => x.UserName == claimsPrincipal.Identity.Name);
            var author = await _userManager.FindByIdAsync(id);
            if (user == null || author == null) return;
            user.Subscriptions.Add(new Subscription
            {
                AuthorId = id,
                CategoryId = -1,
                Type = "Author"
            });
            _subscriptionRepository.SaveChanges();
        }

        public void UnsubscribeCategory(ClaimsPrincipal claimsPrincipal, int id)
        {
            var user = _userManager.Users
                .Include(x => x.Subscriptions)
                .FirstOrDefault(x => x.UserName == claimsPrincipal.Identity.Name);
            if (user == null) return;
            var sub = user.Subscriptions.FirstOrDefault(x => x.Type == "Category" && x.CategoryId == id);
            if (sub == null) return;
            user.Subscriptions.Remove(sub);
            _subscriptionRepository.RemoveSubscription(sub);
        }

        public void UnsubscribeAuthor(ClaimsPrincipal claimsPrincipal, string id)
        {
            var user = _userManager.Users
                .Include(x => x.Subscriptions)
                .FirstOrDefault(x => x.UserName == claimsPrincipal.Identity.Name);
            if (user == null) return;
            var sub = user.Subscriptions.FirstOrDefault(x => x.Type == "Author" && x.AuthorId == id);
            if (sub == null) return;
            user.Subscriptions.Remove(sub);
            _subscriptionRepository.RemoveSubscription(sub);
        }

        public bool IsUserSubscribedToCategory(ClaimsPrincipal claimsPrincipal, int id)
        {
            var user = _userManager.Users
                .Include(x => x.Subscriptions)
                .FirstOrDefault(x => x.UserName == claimsPrincipal.Identity.Name);
            if (user == null) return false;
            var sub = user.Subscriptions.FirstOrDefault(x => x.Type == "Category" && x.CategoryId == id);
            return sub != null;
        }
        
        public bool IsUserSubscribedToAuthor(ClaimsPrincipal claimsPrincipal, string id)
        {
            var user = _userManager.Users
                .Include(x => x.Subscriptions)
                .FirstOrDefault(x => x.UserName == claimsPrincipal.Identity.Name);
            if (user == null) return false;
            var sub = user.Subscriptions.FirstOrDefault(x => x.Type == "Author" && x.AuthorId == id);
            return sub != null;
        }
    }
}