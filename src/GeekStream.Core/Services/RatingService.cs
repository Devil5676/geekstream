using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using Microsoft.AspNetCore.Identity;

namespace GeekStream.Core.Services
{
    public class RatingService
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IArticleVoteRepository _articleVoteRepository;
        private readonly UserManager<AppUser> _userManager;

        public RatingService(
            IArticleRepository articleRepository,
            IArticleVoteRepository articleVoteRepository,
            UserManager<AppUser> userManager)
        {
            _articleRepository = articleRepository;
            _articleVoteRepository = articleVoteRepository;
            _userManager = userManager;
        }
        
        public async Task<int?> UpdateRating(int articleId, int rating, ClaimsPrincipal claimsPrincipal)
        {
            var article = _articleRepository.GetArticleByIdForRating(articleId);
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            if (article == null || user == null) return null;
            if (Math.Abs(rating) > 1) return null;

            var userPreviousVote = article.ArticleVotes.FirstOrDefault(x => x.VotingUser == user);
            if (userPreviousVote == null)
            {
                article.ArticleVotes.Add(new ArticleVote
                {
                    IsPositive = rating > 0,
                    VotingUser = user
                });
                article.Rating += rating;
            }
            else
            {
                if (userPreviousVote.IsPositive && rating < 0)
                {
                    article.Rating += rating;
                    _articleVoteRepository.DeleteArticleVote(userPreviousVote);
                }
                else if (!userPreviousVote.IsPositive && rating > 0)
                {
                    article.Rating += rating;
                    _articleVoteRepository.DeleteArticleVote(userPreviousVote);
                }
            }
            _articleRepository.SaveChanges();
            return article.Rating;
        }

        public async Task<int> GetUserVote(ClaimsPrincipal claimsPrincipal, int articleId)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            if (user == null) return 0;
            var vote = _articleVoteRepository.GetArticleVoteByUser(user, articleId);
            if (vote == null) return 0;

            return vote.IsPositive ? 1 : -1;
        }
    }
}