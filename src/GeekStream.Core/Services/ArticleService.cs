using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace GeekStream.Core.Services
{
    public class ArticleService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IArticleRepository _articleRepository;
        private readonly IKeywordRepository _keywordRepository;
        private readonly RatingService _ratingService;
        private readonly ReviewService _reviewService;
        private readonly UserManager<AppUser> _userManager;

        public ArticleService(
            ICategoryRepository categoryRepository,
            IArticleRepository articleRepository,
            IKeywordRepository keywordRepository,
            RatingService ratingService,
            ReviewService reviewService,
            UserManager<AppUser> userManager)
        {
            _categoryRepository = categoryRepository;
            _articleRepository = articleRepository;
            _keywordRepository = keywordRepository;
            _ratingService = ratingService;
            _reviewService = reviewService;
            _userManager = userManager;
        }

        public async Task<int> CreateArticle(CreateArticleViewModel model, ClaimsPrincipal claims, bool isDraft)
        {
            var user = await _userManager.GetUserAsync(claims);
            var category = _categoryRepository.GetCategoryById(model.CategoryId);
            var inputKeywords = model.Keywords.Split(",");
            
            if (category == null || user == null) return -1;
            var article = new Article
            {
                Title = model.Title,
                Content = model.Content,
                Rating = 0,
                CreatedDate = DateTime.UtcNow,
                PublishedDate = null,
                Author = user,
                Category = category,
                IsDraft = isDraft,
                Keywords = new List<Keyword>(),
                Comments = new List<Comment>(),
                ArticleVotes = new List<ArticleVote>()
            };

            foreach (var keyword in inputKeywords)
            {
                var kw = _keywordRepository.GetKeywordByName(keyword.Trim());
                if (kw != null)
                {
                    article.Keywords.Add(kw);    
                }
                else
                {
                    article.Keywords.Add(new Keyword
                    {
                        Name = keyword.Trim(),
                        NormalizedName = keyword.Trim().ToUpper()
                    });   
                }
            }
            
            _articleRepository.AddArticle(article);
            return article.Id;
        }

        public async Task<IEnumerable<ArticlesViewModel>> GetArticles(ClaimsPrincipal claimsPrincipal)
        {
            var articles = _articleRepository.GetArticles();
            List<ArticlesViewModel> result = new List<ArticlesViewModel>();
            foreach (var article in articles)
            {
                int vote = await _ratingService.GetUserVote(claimsPrincipal, article.Id);
                result.Add(new ArticlesViewModel
                {
                    Id = article.Id,
                    Author = article.Author,
                    Category = article.Category,
                    Comments = article.Comments,
                    Content = article.Content,
                    CreatedDate = article.CreatedDate,
                    Keywords = article.Keywords,
                    PublishedDate = article.PublishedDate,
                    Rating = article.Rating,
                    Title = article.Title,
                    Vote = vote
                });
            }
            
            return result;
        }
        public async Task<IEnumerable<ArticlesViewModel>> GetArticles(ClaimsPrincipal claimsPrincipal, string search)
        {
            var articles = _articleRepository.SearchArticles(search);
            List<ArticlesViewModel> result = new List<ArticlesViewModel>();
            foreach (var article in articles)
            {
                int vote = await _ratingService.GetUserVote(claimsPrincipal, article.Id);
                result.Add(new ArticlesViewModel
                {
                    Id = article.Id,
                    Author = article.Author,
                    Category = article.Category,
                    Comments = article.Comments,
                    Content = article.Content,
                    CreatedDate = article.CreatedDate,
                    Keywords = article.Keywords,
                    PublishedDate = article.PublishedDate,
                    Rating = article.Rating,
                    Title = article.Title,
                    Vote = vote
                });
            }
            
            return result;
        }

        public async Task<DraftViewModel> GetDraftViewModelById(int id, ClaimsPrincipal claimsPrincipal)
        {
            var article = GetArticleById(id);
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            if (article == null || article.Author != user) return null;
            
            string keywords = "";
            foreach (var word in article.Keywords)
            {
                keywords += word.Name + ", ";
            }

            keywords = keywords.Substring(0, keywords.Length - 2);
            
            return new DraftViewModel
            {
                Id = article.Id,
                CategoryId = article.Category.Id,
                Content = article.Content,
                Keywords = keywords,
                Title = article.Title
            };
        }

        public async Task<IEnumerable<DraftsViewModel>> GetUserDrafts(ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            var articles = _articleRepository.GetUserDrafts(user);
            List<DraftsViewModel> drafts = new List<DraftsViewModel>();
            foreach (var article in articles)
            {
                drafts.Add(new DraftsViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Category = article.Category,
                    Content = article.Content,
                    CreatedDate = article.CreatedDate,
                    Keywords = article.Keywords
                });
            }

            return drafts;
        }

        public bool UpdateDraft(DraftViewModel model)
        {
            var article = GetArticleById(model.Id);
            if (article == null) return false;
            var inputKeywords = model.Keywords.Split(",");
            article.Keywords.Clear();
            foreach (var keyword in inputKeywords)
            {
                var kw = _keywordRepository.GetKeywordByName(keyword.Trim());
                if (kw != null)
                {
                    article.Keywords.Add(kw);    
                }
                else
                {
                    article.Keywords.Add(new Keyword
                    {
                        Name = keyword.Trim(),
                        NormalizedName = keyword.Trim().ToUpper()
                    });   
                }
            }
            
            article.Title = model.Title;
            article.Category = _categoryRepository.GetCategoryById(model.CategoryId);
            article.Content = model.Content;
            _articleRepository.SaveChanges();
            return true;
        }

        public int Publish(int reviewId)
        {
            var review = _reviewService.GetReviewById(reviewId);
            if (review == null) return -1;
            if (!review.Approvals.Any()) return -1;
            review.Status = "Опубликована";
            review.Article.PublishedDate = DateTime.UtcNow;
            _articleRepository.SaveChanges();
            return review.Article.Id;
        }
        public int Unpublish(int articleId)
        {
            var article = _articleRepository.GetArticleById(articleId);
            var review = _reviewService.GetReviewByArticleId(articleId);
            if (article == null || review == null) return -1;
            article.IsDraft = true;
            article.PublishedDate = null;
            review.Status = "Снято с публикации";
            _articleRepository.SaveChanges();
            return review.Article.Id;
        }

        public void Remove(Article article)
        {
            var review = _reviewService.GetReviewByArticleId(article.Id);
            if (review != null)
            {
                _reviewService.RemoveReview(review);   
            }
            _articleRepository.RemoveArticle(article);
        }
        
        public Article GetArticleById(int id)
        {
            return _articleRepository.GetArticleById(id);
        }
    }
}