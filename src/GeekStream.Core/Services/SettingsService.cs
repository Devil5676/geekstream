using System;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace GeekStream.Core.Services
{
    public class SettingsService
    {
        private readonly UserManager<AppUser> _userManager;

        public SettingsService(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        
        public async Task<ChangePersonalDataViewModel> GetUserPersonalData(
            ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            var userPersonalData = new ChangePersonalDataViewModel()
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            return userPersonalData;
        }

        public async Task<IdentityResult> ChangeUserPersonalData(
            ChangePersonalDataViewModel model, 
            ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            user.Email = model.Email;
            user.UserName = model.Email;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;

            return await _userManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> ChangeUserPassword(
            ChangePasswordViewModel model, 
            ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            return await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
        }

        public async Task<IdentityResult> ChangeUserAvatar(ClaimsPrincipal claimsPrincipal, ChangeAvatarViewModel model, string webrootPath)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            
            if (user.AvatarPath != null)
            {
                File.Delete(Path.Combine(webrootPath, user.AvatarPath));
                user.AvatarPath = null;
            }
            
            var uploadFolder = Path.Combine(webrootPath, "imgs/avatars");
            var fileName = user.UserName + Path.GetExtension(model.Avatar.FileName);
            var filePath = Path.Combine(uploadFolder, fileName);
            await using (var fs = new FileStream(filePath, FileMode.Create))
            {
                await model.Avatar.CopyToAsync(fs);
            }

            user.AvatarPath = $"imgs/avatars/{fileName}";
            return await _userManager.UpdateAsync(user);
        }
    }
}