using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace GeekStream.Core.Services
{
    public class CommentService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IArticleRepository _articleRepository;
        private readonly IReviewRepository _reviewRepository;

        public CommentService(
            UserManager<AppUser> userManager, 
            IArticleRepository articleRepository,
            IReviewRepository reviewRepository)
        {
            _userManager = userManager;
            _articleRepository = articleRepository;
            _reviewRepository = reviewRepository;
        }
        public async Task SendComment(SendCommentViewModel model, ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            var article = _articleRepository.GetArticleById(model.ArticleId);
            
            if (article == null || user == null) return;

            if (article.Comments == null)
            {
                article.Comments = new List<Comment>();
            }
            
            article.Comments.Add(new Comment
            {
                Author = user,
                Text = model.Text
            });
            
            _articleRepository.SaveChanges();
        }
        public async Task SendReviewComment(SendReviewCommentViewModel model, ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            var review = _reviewRepository.GetReviewById(model.ReviewId);
            
            if (review == null || user == null) return;

            if (review.Comments == null)
            {
                review.Comments = new List<ReviewComment>();
            }
            
            review.Comments.Add(new ReviewComment()
            {
                Author = user,
                Text = model.Text
            });
            
            _reviewRepository.SaveChanges();
        }
    }
}