using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace GeekStream.Core.Services
{
    public class AccountService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public AccountService(UserManager<AppUser> userManager, 
                              SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<IEnumerable<IdentityError>> Register(RegisterViewModel model)
        {
            var user = new AppUser()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = model.Email,
                AvatarPath = null
            };
            
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "User");
                
                // TODO Generate token and send email confirmation link to user. Display email sent message
                
                await _signInManager.SignInAsync(user, isPersistent: true);
                return null;
            }

            return result.Errors;
        }
        public async Task<bool> Login(LoginViewModel model)
        {
            var result = await _signInManager.PasswordSignInAsync(
                userName: model.Email, 
                password: model.Password, 
                isPersistent: true, 
                lockoutOnFailure: false);

            return result.Succeeded;
        }

        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<string> ForgotPassword(ForgotPasswordViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            
            if (user != null)
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var link = $"https://localhost:5001/Account/PasswordReset?email={model.Email}&token={token}";
                // TODO Send reset link to user email
                return token;
            }

            return null;
        }
    }
}