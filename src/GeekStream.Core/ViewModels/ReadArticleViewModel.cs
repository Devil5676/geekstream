using System.ComponentModel.DataAnnotations;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.ViewModels
{
    public class ReadArticleViewModel
    {
        public Article Article { get; set; }
        [Required(ErrorMessage = "Нельзя оставлять пустые комментарии")]
        public string Comment { get; set; }
    }
}