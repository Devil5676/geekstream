using System.ComponentModel.DataAnnotations;

namespace GeekStream.Core.ViewModels
{
    public class DraftViewModel
    {
        [Required]
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Поле заголовок не может быть пустым")]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        
        [Required(ErrorMessage = "Статья должна быть в категории")]
        [Display(Name = "Категория")]
        public int CategoryId { get; set; }
        
        [Display(Name = "Ключевые слова")]
        public string Keywords { get; set; }
        
        [Required(ErrorMessage = "Статья не может быть пустой")]
        [Display(Name = "Текст статьи")]
        public string Content { get; set; }
    }
}