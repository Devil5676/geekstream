using System.ComponentModel.DataAnnotations;

namespace GeekStream.Core.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Поле адреса электронной почты не может быть пустым")]
        [EmailAddress(ErrorMessage = "Некорректный адрес электронной почты")]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Поле пароль не может быть пустым")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
    }
}