namespace GeekStream.Core.ViewModels
{
    public class SendReviewCommentViewModel
    {
        public int ReviewId { get; set; }
        public string Text { get; set; }
    }
}