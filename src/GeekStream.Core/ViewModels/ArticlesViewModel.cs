using System;
using System.Collections.Generic;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.ViewModels
{
    public class ArticlesViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
        public int Vote { get; set; }
        public AppUser Author { get; set; }
        public Category Category { get; set; }
        public ICollection<Keyword> Keywords { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public DateTime? PublishedDate { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    }
}