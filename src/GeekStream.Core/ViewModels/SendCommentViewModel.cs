namespace GeekStream.Core.ViewModels
{
    public class SendCommentViewModel
    {
        public int ArticleId { get; set; }
        public string Text { get; set; }
    }
}