using System.ComponentModel.DataAnnotations;

namespace GeekStream.Core.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Поле адреса электронной почты не может быть пустым")]
        [EmailAddress(ErrorMessage = "Некорректный адрес электронной почты")]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
    }
}