using System;
using System.Collections.Generic;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.ViewModels
{
    public class DraftsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Category Category { get; set; }
        public ICollection<Keyword> Keywords { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}