using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace GeekStream.Core.ViewModels
{
    public class ChangeAvatarViewModel
    {
        [Required(ErrorMessage = "Вы ничего не загрузили")]
        [Display(Name = "Аватар")]
        public IFormFile Avatar { get; set; }
    }
}