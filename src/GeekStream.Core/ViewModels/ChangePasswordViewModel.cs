using System.ComponentModel.DataAnnotations;

namespace GeekStream.Core.ViewModels
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Поле старый пароль не может быть пустым")]
        [DataType(DataType.Password)]
        [Display(Name = "Предыдущий пароль")]
        public string OldPassword { get; set; }
        
        [Required(ErrorMessage = "Поле пароль не может быть пустым")]
        [DataType(DataType.Password)]
        [Display(Name = "Придумайте новый пароль")]
        public string NewPassword { get; set; }
        
        [Required(ErrorMessage = "Поле повтора пароля не может быть пустым")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Пароли не совпадают")]
        [Display(Name = "Повторите пароль ещё раз")]
        public string ConfirmPassword { get; set; }
    }
}