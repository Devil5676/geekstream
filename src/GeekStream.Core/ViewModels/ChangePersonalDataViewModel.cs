using System.ComponentModel.DataAnnotations;

namespace GeekStream.Core.ViewModels
{
    public class ChangePersonalDataViewModel
    {
        [Required(ErrorMessage = "Поле имя не может быть пустым")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "Поле фамилия не может быть пустым")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        
        [Required(ErrorMessage = "Поле адреса электронной почты не может быть пустым")]
        [EmailAddress(ErrorMessage = "Некорректный адрес электронной почты")]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
    }
}