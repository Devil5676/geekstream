using System.Threading.Tasks;
using GeekStream.Core.Services;
using Microsoft.AspNetCore.SignalR;

namespace GeekStream.Hubs
{
    public class RatingHub : Hub
    {
        private readonly RatingService _ratingService;

        public RatingHub(
            RatingService ratingService)
        {
            _ratingService = ratingService;
        }
        
        public async Task Vote(int articleId, int rating)
        {
            var result = await _ratingService.UpdateRating(articleId, rating, Context.User);
            var userVote = await _ratingService.GetUserVote(Context.User, articleId);
            if (result == null) return;
            await Clients.All.SendAsync("UpdateRating", articleId, result, userVote);
        }
    }
}