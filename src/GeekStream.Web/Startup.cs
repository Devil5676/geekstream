using GeekStream.Core.Services;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using GeekStream.Hubs;
using GeekStream.Infrastructure;
using GeekStream.Infrastructure.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GeekStream
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options => 
                options.UseMySql(
                    Configuration.GetConnectionString("DefaultConnection"), 
                    new MySqlServerVersion("5.7.26")));
            
            services.AddControllersWithViews();
            services.AddIdentity<AppUser, IdentityRole>()
                    .AddSignInManager<SignInManager<AppUser>>()
                    .AddEntityFrameworkStores<AppDbContext>();

            services.AddScoped<IArticleRepository, ArticleRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IKeywordRepository, KeywordRepository>();
            services.AddScoped<IArticleVoteRepository, ArticleVoteRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();
            services.AddScoped<ISubscriptionRepository, SubscriptionRepository>();
            
            services.AddScoped<AccountService>();
            services.AddScoped<CategoryService>();
            services.AddScoped<CommentService>();
            services.AddScoped<SubscriptionService>();
            services.AddScoped<RatingService>();
            services.AddScoped<ReviewService>();
            services.AddScoped<SettingsService>();
            services.AddScoped<ArticleService>();
            services.AddScoped<RatingService>();

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Articles}/{id?}");
                endpoints.MapHub<RatingHub>("/rating");
            });
        }
    }
}