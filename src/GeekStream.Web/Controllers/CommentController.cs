using System.Threading.Tasks;
using GeekStream.Core.Services;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GeekStream.Controllers
{
    public class CommentController : Controller
    {
        private readonly CommentService _commentService;

        public CommentController(CommentService commentService)
        {
            _commentService = commentService;
        }
        
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SendComment(SendCommentViewModel model)
        {
            if (!ModelState.IsValid) return RedirectToAction("Articles", "Home");
            
            await _commentService.SendComment(model, User);
            
            return RedirectToAction("Read", "Article", new {id = model.ArticleId});
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SendReviewComment(SendReviewCommentViewModel model)
        {
            if (!ModelState.IsValid) return RedirectToAction("Review", "Review", new { id = model.ReviewId});

            await _commentService.SendReviewComment(model, User);
            
            return RedirectToAction("Review", "Review", new {id = model.ReviewId});
        }
    }
}