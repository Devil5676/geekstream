using System.Threading.Tasks;
using GeekStream.Core.Services;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace GeekStream.Controllers
{
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly SettingsService _settingsService;
        private readonly IWebHostEnvironment _environment;

        public SettingsController(SettingsService settingsService, IWebHostEnvironment environment)
        {
            _settingsService = settingsService;
            _environment = environment;
        }
        
        [HttpGet]
        public async Task<IActionResult> ChangePersonalData()
        {
            return View(await _settingsService.GetUserPersonalData(User));
        }
        
        [HttpPost]
        public async Task<IActionResult> ChangePersonalData(ChangePersonalDataViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            
            var result = await _settingsService.ChangeUserPersonalData(model, User);
            if (result.Succeeded)
            {
                return RedirectToAction("ChangePersonalData");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            
            return View();
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid) return View();

            var result = await _settingsService.ChangeUserPassword(model, User);
            if (result.Succeeded)
            {
                return RedirectToAction("ChangePassword");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            
            return View();
        }

        [HttpGet]
        public IActionResult ChangeAvatar()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> ChangeAvatar(ChangeAvatarViewModel model)
        {
            if (!ModelState.IsValid) return View();

            var result = await _settingsService.ChangeUserAvatar(User, model, _environment.WebRootPath);
            if (result.Succeeded)
            {
                return RedirectToAction("ChangeAvatar");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            
            return View();
        }
    }
}