﻿using System.Diagnostics;
using System.Threading.Tasks;
using GeekStream.Core.Services;
using GeekStream.Models;
using Microsoft.AspNetCore.Mvc;

namespace GeekStream.Controllers
{
    public class HomeController : Controller
    {
        private readonly ArticleService _articleService;

        public HomeController(ArticleService articleService)
        {
            _articleService = articleService;
        }
        
        [HttpGet]
        public async Task<IActionResult> Articles()
        {
            return View( await _articleService.GetArticles(User));
        }

        [HttpPost]
        public async Task<IActionResult> Search(string search)
        {
            return View("Articles", await _articleService.GetArticles(User, search));
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}