using System.Threading.Tasks;
using GeekStream.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GeekStream.Controllers
{
    public class ReviewController : Controller
    {
        private readonly ReviewService _reviewService;
        private readonly CategoryService _categoryService;
        private readonly ArticleService _articleService;

        public ReviewController(
            ReviewService reviewService,
            CategoryService categoryService,
            ArticleService articleService)
        {
            _reviewService = reviewService;
            _categoryService = categoryService;
            _articleService = articleService;
        }
        
        [HttpGet]
        [Authorize]
        public IActionResult Review(int id)
        {
            var review = _reviewService.GetReviewById(id);
            if (review == null) return RedirectToAction("Articles", "Home");
            if (User.Identity.Name == review.Author.UserName 
                || User.IsInRole("Mod") 
                || User.IsInRole("Admin"))
                return View(review);
            return RedirectToAction("Articles", "Home");
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateReview(int articleId)
        {
            var reviewId = _reviewService.CreateReview(articleId);
            return RedirectToAction("Review", new { id = reviewId });
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Mod")]
        public IActionResult RejectReview(int reviewId)
        {
            _reviewService.RejectReview(reviewId);
            return RedirectToAction("Review", "Review", new { id = reviewId });
        }
        
        [HttpPost]
        [Authorize(Roles = "Admin, Mod")]
        public async Task<IActionResult> ApproveReview(int reviewId)
        {
            await _reviewService.ApproveReview(reviewId, User);
            return RedirectToAction("Review", "Review", new { id = reviewId });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> MyReviews()
        {
            var reviews = await _reviewService.GetUserReviews(User);
            return View(reviews);
        }
        
        [HttpGet]
        [Authorize(Roles = "Admin, Mod")]
        public IActionResult All()
        {
            var reviews = _reviewService.GetAllReviews();
            return View(reviews);
        }
    }
}