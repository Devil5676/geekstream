using System.Threading.Tasks;
using GeekStream.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GeekStream.Controllers
{
    public class SubscriptionController : Controller
    {
        private readonly SubscriptionService _subscriptionService;

        public SubscriptionController(
            SubscriptionService subscriptionService)
        {
            _subscriptionService = subscriptionService;
        }
        
        [HttpPost]
        [Authorize]
        public IActionResult SubscribeCategory(int categoryId)
        {
            _subscriptionService.SubscribeCategory(User, categoryId);
            return RedirectToAction("Show", "Category", new { id = categoryId });
        }
        
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SubscribeAuthor(string authorId)
        {
            await _subscriptionService.SubscribeAuthor(User, authorId);
            return RedirectToAction("Show", "Author", new { id = authorId });
        }
        
        [HttpPost]
        [Authorize]
        public IActionResult UnsubscribeCategory(int categoryId)
        {
            _subscriptionService.UnsubscribeCategory(User, categoryId);
            return RedirectToAction("Show", "Category", new { id = categoryId });
        }
        
        [HttpPost]
        [Authorize]
        public IActionResult UnsubscribeAuthor(string authorId)
        {
            _subscriptionService.UnsubscribeAuthor(User, authorId);
            return RedirectToAction("Show", "Author", new { id = authorId});
        }
    }
}