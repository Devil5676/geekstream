using GeekStream.Core.Shared.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GeekStream.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryController(
            ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        
        [HttpGet]
        public IActionResult Show(int id)
        {
            var category = _categoryRepository.GetCategoryById(id);
            return View(category);
        }
        
        [HttpGet]
        public IActionResult List()
        {
            var categories = _categoryRepository.GetCategories();
            return View(categories);
        }
    }
}