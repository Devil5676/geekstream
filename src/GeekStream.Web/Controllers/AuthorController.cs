using System.Linq;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GeekStream.Controllers
{
    public class AuthorController : Controller
    {
        private readonly UserManager<AppUser> _userManager;

        public AuthorController(
            UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        
        [HttpGet]
        public async Task<IActionResult> Show(string id)
        {
            var user = await _userManager.Users
                .Include(x => x.Articles)
                    .ThenInclude(x => x.Category)
                    .AsSplitQuery()
                .Include(x => x.Articles)
                    .ThenInclude(x => x.Comments)
                    .AsSplitQuery()
                .Include(x => x.Articles)
                    .ThenInclude(x => x.ArticleVotes)
                    .AsSplitQuery()
                .FirstOrDefaultAsync(x => x.Id == id); 
            return View(user);
        }

        [HttpGet]
        public IActionResult List()
        {
            var users = _userManager.Users.ToList();
            return View(users);
        }
    }
}