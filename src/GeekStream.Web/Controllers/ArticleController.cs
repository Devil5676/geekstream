using System.Threading.Tasks;
using GeekStream.Core.Services;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GeekStream.Controllers
{
    public class ArticleController : Controller
    {
        private readonly CategoryService _categoryService;
        private readonly ArticleService _articleService;
        private readonly ReviewService _reviewService;

        public ArticleController(
            CategoryService categoryService,
            ArticleService articleService,
            ReviewService reviewService)
        {
            _categoryService = categoryService;
            _articleService = articleService;
            _reviewService = reviewService;
        }
        
        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            ViewBag.Categories = _categoryService.GetCategories();
            return View();
        }
        
        [HttpGet]
        public IActionResult Read(int id)
        {
            var article = _articleService.GetArticleById(id);
            return View(new ReadArticleViewModel
            {
                Article = article
            });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(CreateArticleViewModel model)
        {
            ViewBag.Categories = _categoryService.GetCategories();
            if (!ModelState.IsValid) return View();
            var articleId = await _articleService.CreateArticle(model, User, false);
            if (articleId < 0)
            {
                ModelState.AddModelError("", "Не удалось создать статью");
                return View();
            }

            var reviewId = _reviewService.CreateReview(articleId);
            return RedirectToAction("Review", "Review", new { id = reviewId});
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Draft(int id)
        {
            ViewBag.Categories = _categoryService.GetCategories();
            var model = await _articleService.GetDraftViewModelById(id, User);
            if (model == null) return NotFound();
            return View(model);
        }
        
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> MyDrafts()
        {
            var model = await _articleService.GetUserDrafts(User);
            if (model == null) return NotFound();
            return View(model);
        }
        
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SaveAsDraft(CreateArticleViewModel model)
        {
            ViewBag.Categories = _categoryService.GetCategories();
            if (!ModelState.IsValid) return RedirectToAction("Articles", "Home");
            var articleId = await _articleService.CreateArticle(model, User, true);
            return RedirectToAction("Draft", new {id = articleId});
        }
        
        [HttpPost]
        [Authorize]
        public IActionResult SaveDraft(DraftViewModel model)
        {
            if (!ModelState.IsValid) return RedirectToAction("Articles", "Home");
            var articleId = model.Id;
            _articleService.UpdateDraft(model);
            return RedirectToAction("Draft", new {id = articleId});
        }
        
        [HttpPost]
        [Authorize]
        public IActionResult Publish(int reviewId)
        {
            if (!ModelState.IsValid) return RedirectToAction("Articles", "Home");
            var articleId = _articleService.Publish(reviewId);
            return RedirectToAction("Read", new {id = articleId});
        }
        
        [HttpGet]
        [Authorize]
        public IActionResult Unpublish(int articleId)
        {
            if (!ModelState.IsValid) return RedirectToAction("Articles", "Home");
            var article = _articleService.GetArticleById(articleId);
            if (User.Identity.Name != article.Author.UserName && !User.IsInRole("Admin") && !User.IsInRole("Mod"))
                return RedirectToAction("Articles", "Home");
            _articleService.Unpublish(articleId);
            return RedirectToAction("Draft", new {id = articleId});
        }
        
        [HttpPost]
        [Authorize]
        public IActionResult Remove(int id)
        {
            if (!ModelState.IsValid) return RedirectToAction("Articles", "Home");
            var article = _articleService.GetArticleById(id);
            if (article == null || !article.IsDraft && article.PublishedDate != null) return RedirectToAction("Articles", "Home");
            if (User.Identity.Name != article.Author.UserName && !User.IsInRole("Admin") && !User.IsInRole("Mod"))
                return RedirectToAction("Articles", "Home");
            _articleService.Remove(article);
            return RedirectToAction("Articles", "Home");
        }
    }
}