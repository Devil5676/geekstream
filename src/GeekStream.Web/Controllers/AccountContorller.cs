﻿using System.Threading.Tasks;
using GeekStream.Core.Services;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GeekStream.Controllers
{
    public class AccountController : Controller
    {
        private readonly AccountService _accountService;

        public AccountController(AccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var errors = await _accountService.Register(model);

            if (errors == null)
            {
                return RedirectToAction("Articles", "Home");
            }

            foreach (var err in errors)
            {
                ModelState.AddModelError("", err.Description);
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var result = await _accountService.Login(model);

            if (result)
            {
                return RedirectToAction("Articles", "Home");
            }
            ModelState.AddModelError("", "Неверный логин или пароль");
            
            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult Logout()
        {
            _accountService.Logout();
            return RedirectToAction("Articles", "Home");
        }

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            _accountService.ForgotPassword(model);
            
            return RedirectToAction("Articles", "Home");
        }
    }
}