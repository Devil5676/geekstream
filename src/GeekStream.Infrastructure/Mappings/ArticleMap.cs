using GeekStream.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStream.Infrastructure.Mappings
{
    public class ArticleMap : IEntityTypeConfiguration<Article>
    {
        public void Configure(EntityTypeBuilder<Article> builder)
        {
            builder
                .HasOne(e => e.Author)
                .WithMany(e => e.Articles);

            builder
                .HasMany(x => x.Keywords)
                .WithMany(x => x.Articles)
                .UsingEntity(j => j.ToTable("ArticleKeyword"));
        }
    }
}