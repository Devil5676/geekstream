﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GeekStream.Infrasructure.Migrations
{
    public partial class Avatars : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1226720d-47e6-4700-bfdb-071c6750b438");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "21b85f2b-62be-4eca-8365-00a58e1199af");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d71a1575-fc09-48b0-8393-397c555066d6");

            migrationBuilder.AddColumn<string>(
                name: "AvatarPath",
                table: "AspNetUsers",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "0c570c3b-27fb-4730-a840-f0bdd855b979", "95943b64-44e9-4908-8979-f9511ce72cc4", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "01b5cd03-c1e4-44bd-8f3c-b8d499c05ac6", "9fd1114f-172e-429a-94d0-f0d935f779ef", "Mod", "MOD" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "f754b6d1-6bc9-421f-8323-b19dbf394f52", "12571ac0-9e93-4e4c-92dc-a7cc756101a9", "User", "USER" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "01b5cd03-c1e4-44bd-8f3c-b8d499c05ac6");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0c570c3b-27fb-4730-a840-f0bdd855b979");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f754b6d1-6bc9-421f-8323-b19dbf394f52");

            migrationBuilder.DropColumn(
                name: "AvatarPath",
                table: "AspNetUsers");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "21b85f2b-62be-4eca-8365-00a58e1199af", "7fa36983-a979-4946-bc82-b1d211d0eeb0", "Administrator", null });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "d71a1575-fc09-48b0-8393-397c555066d6", "b0587f2f-5443-4f7e-b43a-a4eada4d67b7", "Moderator", null });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "1226720d-47e6-4700-bfdb-071c6750b438", "eaba69a4-c4ea-4db6-90f2-eb01ec6954f2", "User", null });
        }
    }
}
