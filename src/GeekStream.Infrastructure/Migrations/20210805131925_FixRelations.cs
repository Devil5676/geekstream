﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GeekStream.Infrasructure.Migrations
{
    public partial class FixRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArticleKeyword_Articles_ArticleId",
                table: "ArticleKeyword");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleKeyword_Keywords_KeywordId",
                table: "ArticleKeyword");

            migrationBuilder.RenameColumn(
                name: "KeywordId",
                table: "ArticleKeyword",
                newName: "KeywordsId");

            migrationBuilder.RenameColumn(
                name: "ArticleId",
                table: "ArticleKeyword",
                newName: "ArticlesId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleKeyword_KeywordId",
                table: "ArticleKeyword",
                newName: "IX_ArticleKeyword_KeywordsId");

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleKeyword_Articles_ArticlesId",
                table: "ArticleKeyword",
                column: "ArticlesId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleKeyword_Keywords_KeywordsId",
                table: "ArticleKeyword",
                column: "KeywordsId",
                principalTable: "Keywords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArticleKeyword_Articles_ArticlesId",
                table: "ArticleKeyword");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleKeyword_Keywords_KeywordsId",
                table: "ArticleKeyword");

            migrationBuilder.RenameColumn(
                name: "KeywordsId",
                table: "ArticleKeyword",
                newName: "KeywordId");

            migrationBuilder.RenameColumn(
                name: "ArticlesId",
                table: "ArticleKeyword",
                newName: "ArticleId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleKeyword_KeywordsId",
                table: "ArticleKeyword",
                newName: "IX_ArticleKeyword_KeywordId");

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleKeyword_Articles_ArticleId",
                table: "ArticleKeyword",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleKeyword_Keywords_KeywordId",
                table: "ArticleKeyword",
                column: "KeywordId",
                principalTable: "Keywords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
