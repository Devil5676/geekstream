﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GeekStream.Infrasructure.Migrations
{
    public partial class FixRememberVotes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArticleVote_Articles_ArticleId",
                table: "ArticleVote");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleVote_AspNetUsers_VotingUserId",
                table: "ArticleVote");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ArticleVote",
                table: "ArticleVote");

            migrationBuilder.RenameTable(
                name: "ArticleVote",
                newName: "ArticleVotes");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleVote_VotingUserId",
                table: "ArticleVotes",
                newName: "IX_ArticleVotes_VotingUserId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleVote_ArticleId",
                table: "ArticleVotes",
                newName: "IX_ArticleVotes_ArticleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArticleVotes",
                table: "ArticleVotes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleVotes_Articles_ArticleId",
                table: "ArticleVotes",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleVotes_AspNetUsers_VotingUserId",
                table: "ArticleVotes",
                column: "VotingUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArticleVotes_Articles_ArticleId",
                table: "ArticleVotes");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleVotes_AspNetUsers_VotingUserId",
                table: "ArticleVotes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ArticleVotes",
                table: "ArticleVotes");

            migrationBuilder.RenameTable(
                name: "ArticleVotes",
                newName: "ArticleVote");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleVotes_VotingUserId",
                table: "ArticleVote",
                newName: "IX_ArticleVote_VotingUserId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleVotes_ArticleId",
                table: "ArticleVote",
                newName: "IX_ArticleVote_ArticleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArticleVote",
                table: "ArticleVote",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleVote_Articles_ArticleId",
                table: "ArticleVote",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleVote_AspNetUsers_VotingUserId",
                table: "ArticleVote",
                column: "VotingUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
