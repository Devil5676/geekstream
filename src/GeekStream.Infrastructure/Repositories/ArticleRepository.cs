using System.Collections.Generic;
using System.Linq;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using Microsoft.EntityFrameworkCore;

namespace GeekStream.Infrastructure.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly AppDbContext _context;

        public ArticleRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public IEnumerable<Article> GetArticles()
        {
            return _context.Articles
                .Include(x => x.Author)
                .Include(x => x.Keywords)
                    .AsSplitQuery()
                .Include(x => x.Category)
                    .AsSplitQuery()
                .Include(x => x.Comments)
                    .AsSplitQuery()
                .Include(x => x.ArticleVotes)
                    .AsSplitQuery()
                .Where(x => !x.IsDraft && x.PublishedDate != null)
                .OrderByDescending(x => x.PublishedDate)
                .ToList();
        }

        public IEnumerable<Article> SearchArticles(string search)
        {
            return _context.Articles
                .Include(x => x.Author)
                .Include(x => x.Keywords)
                    .AsSplitQuery()
                .Include(x => x.Category)
                    .AsSplitQuery()
                .Include(x => x.Comments)
                    .AsSplitQuery()
                .Include(x => x.ArticleVotes)
                    .AsSplitQuery()
                .Where(x => !x.IsDraft && x.PublishedDate != null && x.Title.Contains(search))
                .OrderByDescending(x => x.PublishedDate)
                .ToList();
        }

        public Article GetArticleById(int id)
        {
            return _context.Articles
                .Include(x => x.Author)
                .Include(x => x.Category)
                .Include(x => x.Keywords)
                    .AsSplitQuery()
                .Include(x => x.Comments)
                    .ThenInclude(x => x.Author)
                .FirstOrDefault(x => x.Id == id);
        }

        public Article GetArticleByIdForRating(int id)
        {
            return _context.Articles
                .Include(x => x.ArticleVotes)
                .FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Article> GetUserDrafts(AppUser user)
        {
            return _context.Articles
                .Include(x => x.Keywords)
                .Include(x => x.Category)
                .OrderByDescending(x => x.CreatedDate)
                .Where(x => x.Author == user && x.IsDraft)
                .ToList();
        }

        public void AddArticle(Article article)
        { 
            _context.Articles.Add(article);
            _context.SaveChanges();
        }

        public void RemoveArticle(Article article)
        {
            _context.Articles.Remove(article);
            _context.SaveChanges();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}