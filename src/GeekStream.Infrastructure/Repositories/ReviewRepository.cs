using System.Collections.Generic;
using System.Linq;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using Microsoft.EntityFrameworkCore;

namespace GeekStream.Infrastructure.Repositories
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly AppDbContext _context;

        public ReviewRepository(AppDbContext context)
        {
            _context = context;
        }

        public int CreateReview(Review review)
        {
            _context.Add(review);
            _context.SaveChanges();
            return review.Id;
        }

        public Review GetReviewById(int id)
        {
            return _context.Reviews
                .Include(x => x.Article)
                    .ThenInclude(x => x.Category)
                .Include(x => x.Author)
                    .AsSplitQuery()
                .Include(x => x.Approvals)
                    .AsSplitQuery()
                .Include(x => x.Comments)
                    .ThenInclude(x => x.Author)
                .FirstOrDefault(x => x.Id == id);
        }

        public ICollection<Review> GetUserReviews(AppUser user)
        {
            return _context.Reviews
                .Include(x => x.Article)
                    .ThenInclude(x => x.Category)
                .Include(x => x.Author)
                    .AsSplitQuery()
                .Include(x => x.Approvals)
                    .AsSplitQuery()
                .Include(x => x.Comments)
                    .ThenInclude(x => x.Author)
                .Where(x => x.Author == user)
                .ToList();
        }

        public ICollection<Review> GetAllReviews()
        {
            return _context.Reviews
                .Include(x => x.Article)
                    .ThenInclude(x => x.Category)
                .Include(x => x.Author)
                    .AsSplitQuery()
                .Include(x => x.Approvals)
                    .AsSplitQuery()
                .Include(x => x.Comments)
                    .ThenInclude(x => x.Author)
                    .AsSplitQuery()
                .Where(x => x.Status == "Ожидает рассмотрения")
                .OrderByDescending(x => x.ReviewCreated)
                .ToList();
        }

        public Review GetReviewByArticleId(int id)
        {
            return _context.Reviews
                .Include(x => x.Article)
                    .ThenInclude(x => x.Category)
                .Include(x => x.Author)
                    .AsSplitQuery()
                .Include(x => x.Approvals)
                    .AsSplitQuery()
                .Include(x => x.Comments)
                    .ThenInclude(x => x.Author)
                .FirstOrDefault(x => x.Article.Id == id);
        }

        public void RemoveReview(Review review)
        {
            _context.Reviews.Remove(review);
            _context.SaveChanges();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}