using System.Linq;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;

namespace GeekStream.Infrastructure.Repositories
{
    public class ArticleVoteRepository : IArticleVoteRepository
    {
        private readonly AppDbContext _context;

        public ArticleVoteRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public void DeleteArticleVote(ArticleVote articleVote)
        {
            _context.ArticleVotes.Remove(articleVote);
            _context.SaveChanges();
        }

        public ArticleVote GetArticleVoteByUser(AppUser user, int articleId)
        {
            return _context.ArticleVotes.FirstOrDefault(x => x.Article.Id == articleId
                                                      && x.VotingUser == user);
        }
    }
}