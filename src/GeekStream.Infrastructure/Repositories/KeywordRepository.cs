using System.Linq;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;

namespace GeekStream.Infrastructure.Repositories
{
    public class KeywordRepository : IKeywordRepository
    {
        private readonly AppDbContext _context;

        public KeywordRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public Keyword GetKeywordByName(string name)
        {
            return _context.Keywords.FirstOrDefault(x  => x.Name == name);
        }
    }
}