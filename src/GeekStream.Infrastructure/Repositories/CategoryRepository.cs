using System.Collections.Generic;
using System.Linq;
using GeekStream.Core.Shared.Entities;
using GeekStream.Core.Shared.Repositories;
using Microsoft.EntityFrameworkCore;

namespace GeekStream.Infrastructure.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly AppDbContext _context;

        public CategoryRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public IEnumerable<Category> GetCategories()
        {
            return _context.Categories.ToList();
        }

        public Category GetCategoryById(int id)
        {
            return _context.Categories
                .Include(x => x.Articles)
                    .ThenInclude(x => x.Author)
                .Include(x => x.Articles)
                    .ThenInclude(x => x.Comments)
                .FirstOrDefault(x => x.Id == id);
        }
    }
}